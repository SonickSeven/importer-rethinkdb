import * as r from 'rethinkdb'
import {readFileSync} from 'fs'
import Debug from 'debug'

const debug = Debug('importer-rethinkdb')

const folder = './filesDb/'

export default class rethinkdbActions {
	static instance
	conn
	nameTable = ''
	constructor(conn) {
		this.conn = conn
	}
	static async creteDatabase() {
		return new Promise(res => {
			r.connect({
				port: process.env.RETHINKDB_PORT,
				host: process.env.RETHINKDB_HOST,
				password: process.env.RETHINKDB_PASS,
			})
				.then(conn => {
					r.dbList()
						.contains(process.env.RETHINKDB_DB)
						.do(databaseExists =>
							r.branch(
								databaseExists,
								{dbs_created: 0},
								r.dbCreate(process.env.RETHINKDB_DB)
							)
						)
						.run(conn)
						.then(res)
						.catch(debug)
				})
				.catch(() => {
					debug('Database aleady existed')
					res(true)
				})
		})
	}
	setNameTable(name) {
		this.nameTable = name
	}
	getNameTable() {
		return this.nameTable
	}
	async createTable() {
		const resa = await r
			.db(process.env.RETHINKDB_DB)
			.tableList()
			.run(this.conn)
		if (resa.indexOf(this.getNameTable()) < 0) {
			await r
				.tableCreate(this.getNameTable())
				.run(this.conn)
				.then(() => {
					debug(`table ${this.getNameTable()} created`)
				})
				.catch(err => {
					debug(err, 'iusehrergseg seg')
				})
		}
	}
	async insertDataIntoTable(nameFile) {
		const contentFile = readFileSync(`${folder}${nameFile}`, 'utf8')
		try {
			const dataTable = JSON.parse(contentFile)
			const result = await r
				.table(this.getNameTable())
				.insert(dataTable)
				.run(this.conn)
			if (result.inserted)
				debug(`Data saved in table: ${this.getNameTable()}`)
		} catch (err) {
			debug(`Data from ${nameFile} could be inseted in table`)
		}
	}
	static getInstance(conne) {
		if (!this.instance) this.instance = new this(conne)
		return this.instance
	}
}
