import * as r from 'rethinkdb'
import {exec} from 'child_process'
import Debug from 'debug'

const debug = Debug('importer-rethinkdb')

let rdbs = null

const conn = async () =>
	new Promise(resolve => {
		r.connect({
			db: process.env.RETHINKDB_DB,
			port: process.env.RETHINKDB_PORT,
			host: process.env.RETHINKDB_HOST,
			password: process.env.RETHINKDB_PASS,
		})
			.then(conn => {
				rdbs = conn
				resolve(conn)
			})
			.catch(() => {
				console.log(
					process.env.COMMAND_CONNECT_RETHINKDB,
					'FT niños vayan a pasear en'
				)
				exec(process.env.COMMAND_CONNECT_RETHINKDB, err1 => {
					if (err1) debug(err1, 'error en acceso SSH', __filename)
				})
			})
	})

export function callConnect() {
	return new Promise(res => {
		let county = 0
		function loadConn() {
			if (county < 30) {
				if (!rdbs)
					setTimeout(() => {
						debug('FT intentando conectar con rethinkdb')
						conn()
						county += 1
						loadConn()
					}, 1000)
				else {
					if (!rdbs.open)
						setTimeout(() => {
							conn()
							loadConn()
						}, 1000)
					else {
						debug('FT connectado con rethinkdb')
						res(rdbs)
					}
				}
			} else debug('rethinkdb never connect')
		}
		loadConn()
	})
}
