import {readdirSync} from 'fs'
import Debug from 'debug'

import {callConnect} from './connect'
import rethinkdbActions from './rethinkdbActions'

const debug = Debug('importer-rethinkdb')

const folder = './filesDb/'

class rethink {
	constructor() {}
	static connect
	static async conn() {
		if (!this.connect) this.connect = await callConnect()
		return this.connect
	}
}

class importer {
	conn
	rethinky
	constructor() {}

	async connect() {
		this.conn = await rethink.conn()
		this.rethinky = rethinkdbActions.getInstance(this.conn)
	}

	async loadFiles() {
		try {
			const files = readdirSync(folder)
			if (files.length) {
				for (let i = 0; i < files.length; i++) {
					if (files[i].match(/\.json/gi)) {
						this.rethinky.setNameTable(
							files[i].replace(/.JSON/gi, '')
						)
						await this.rethinky.createTable()
						await this.rethinky.insertDataIntoTable(files[i])
					}
				}
				debug(
					`The process to import backup of rethinkdb files has been finalized`
				)
			} else console.log('FT no hay archivos en esa carpeta')
		} catch (err) {
			console.log(err, 'FT paso un error por acá ome')
		}
	}
}

async function runTime() {
	const runFiles = new importer()
	await runFiles.connect()
	await rethinkdbActions.creteDatabase()
	await runFiles.loadFiles()
}

runTime()
